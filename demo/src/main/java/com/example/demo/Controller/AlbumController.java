package com.example.demo.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.Album;
import com.example.demo.Service.AlbumService;

@RestController
public class AlbumController {
    @Autowired
    private AlbumService albumService;

    @GetMapping("/album-info")
    public Album getRegionInfo(@RequestParam("albumId") int albumId ) {
            ArrayList<Album> albums = albumService.getAllAlbum();
        
            Album allBumFind = new Album();
            for(Album album : albums) {
                if(album.getId() == albumId) {
                    allBumFind = album;
                }
            }
            return allBumFind;
    }
}
