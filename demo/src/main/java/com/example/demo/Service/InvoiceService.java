package com.example.demo.Service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Model.Customer;
import com.example.demo.Model.Invoices;

@Service
public class InvoiceService {
    
    @Autowired
    private CustomerService customerService ;

    public ArrayList<Invoices> getInvoiceList(){
        ArrayList<Invoices> invoices = new ArrayList<Invoices>();;

        Invoices invoice1 = new Invoices(1 , new Customer(1 , "Linh Giang" , 10 ) , 10.0 );
        Invoices invoice2 = new Invoices(2 , new Customer(1 , "Châu Giang" , 20 ) , 20.0 );
        Invoices invoice3 = new Invoices(3 , new Customer(1 , "Trường Giang" , 30 ) , 30.0 );


        invoice1.setCustomer(CustomerService.customer1);
        invoice2.setCustomer(customerService.customer2);
        invoice3.setCustomer(CustomerService.customer3);

        invoices.add(invoice1);
        invoices.add(invoice2);
        invoices.add(invoice3);

        return invoices;
    }
}
