package com.example.demo.Service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Model.Country;
import com.example.demo.Model.Region;


@Service
public class CountryService {

    @Autowired
    RegionService regionsService;

    // tạo 1 object và thêm vào 3 thuộc tính 
    Country VN = new Country("VN", "Việt Nam" , new ArrayList<Region>(){
        {
            add(new Region("60","Bình Dương"));
        }
    });

    // tạo 1 object và thêm 2 thuộc tính , có thể gọi riêng biệt , từng country , từng region 
    Country JP = new Country("JP", "Japan");
    Country US = new Country("US", "United States");

    public ArrayList<Country> getCountryList() {
        ArrayList<Country> countryList = new ArrayList<>();
        
        VN.setRegions(regionsService.getRegionVN());
        JP.setRegions(regionsService.getRegionJP());
        US.setRegions(regionsService.getRegionUS());

        countryList.add(VN);
        countryList.add(JP);
        countryList.add(US);

        return countryList;
    }
}
