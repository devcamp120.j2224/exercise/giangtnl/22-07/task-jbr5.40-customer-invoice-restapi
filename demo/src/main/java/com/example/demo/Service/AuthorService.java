package com.example.demo.Service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.example.demo.Model.Author;

@Service
public class AuthorService {
    
    public static ArrayList<Author> getAuthorVN(){

        ArrayList<Author> authors = new ArrayList<Author>();

        Author authorsVN1 = new Author("Tô Hoài", "tohoai@gmail.com","male");
        Author authorsVN2 = new Author("Nguyễn Nhật Ánh", "nguyennhatanh@gmail.com","male");
        Author authorsVN3 = new Author("Xuân Diệu", "xuandieu@gmail.com","male");

        authors.add(authorsVN1);
        authors.add(authorsVN2);
        authors.add(authorsVN3);

        return authors;
    }
    public static ArrayList<Author> getAuthorUK(){
        ArrayList<Author>authors = new ArrayList<Author>();

        Author authorsUK1 = new Author("J.K.Rowling", "jkrowling@gmail.com","female");
        Author authorsUK2 = new Author("Mark Twain", "marktwain@gmail.com","male");
        Author authorsUK3 = new Author("William Shakespeare", "shakespeare@gmail.com","male");

        authors.add(authorsUK1);
        authors.add(authorsUK2);
        authors.add(authorsUK3);

        return authors;
    }
    public static ArrayList<Author> getAuthorUS(){
        ArrayList<Author> authors = new ArrayList<Author>();

        Author authorsUS1 = new Author("Dan Brown", "danbrown@gmail.com","male");
        Author authorsUS2 = new Author("Ernest Hemingway", "ernesthemingway@gmail.com","male");
        Author authorsUS3 = new Author("William Faulkner", "williamfaulkner@twitter3.com","male");

        authors.add(authorsUS1);
        authors.add(authorsUS2);
        authors.add(authorsUS3);

        return authors;
    }

    public static ArrayList<Author> getAuthorsAll() {
        ArrayList<Author> AuthorsAll = new ArrayList<Author>();

       AuthorsAll.addAll(getAuthorVN());
       AuthorsAll.addAll(getAuthorUK());
       AuthorsAll.addAll(getAuthorUS());
    
        return AuthorsAll;
        
    }
}
